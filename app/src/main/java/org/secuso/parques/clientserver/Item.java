package org.secuso.parques.clientserver;

public class Item {

    private String ip;
    private int turno;

    public Item(String ip, int turno) {
        this.ip = ip;
        this.turno = turno;
    }
    public Item(String ip) {
        this.ip = ip;
        this.turno = turno;
    }
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getTurno() {
        return turno;
    }

    public void setTurno(int turno) {
        this.turno = turno;
    }
}