package org.secuso.parques.clientserver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.secuso.parques.R;
import org.secuso.parques.activities.GameActivity;
import org.secuso.parques.logic.Player;

import java.util.ArrayList;

public class CrearPartida extends Activity implements Server.Interfase {
    Server server;
    TextView infoip, msg;
    ApplicationContext appContext;
    RecyclerView recyclerView;
    Button btn_iniciar;
    ItemArrayAdapter itemArrayAdapter;
    private ArrayList<Player> player_list = new ArrayList<>();
    ArrayList<Server.ConnectionToClient> playerArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_crear_partida);
        infoip = (TextView) findViewById(R.id.infoip);
        appContext = (ApplicationContext) getApplicationContext();
        appContext.creandoPartida=true;
        itemArrayAdapter = new ItemArrayAdapter(R.layout.list_item2, playerArrayList);
        recyclerView = (RecyclerView) findViewById(R.id.item_list);
        btn_iniciar = (Button) findViewById(R.id.btn_iniciar);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(itemArrayAdapter);
        btn_iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                player_list = crearJugadores();

                Intent intent = new Intent(CrearPartida.this, GameActivity.class);
                intent.putParcelableArrayListExtra("Players", player_list);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                appContext.server.sendToAll("iniciar");
            }
        });
        appContext.server = new Server(this,appContext);
        infoip.setText(appContext.server.getIpAddress());
    }
    public ArrayList<Player> crearJugadores(){
        player_list =new ArrayList<>();
        appContext.server.sendToAll("start");
        appContext.creandoPartida=false;
        int[] androidColors = getBaseContext().getResources().getIntArray(R.array.playerColors);
        player_list.add(new Player(0,  R.color.black, "servidor_"+appContext.server.getIpAddress(), false, 1));
        appContext.myplayer_name= player_list.get(0).getName();
        for (int i =0;i<playerArrayList.size();i++){
            player_list.add(new Player(i+1, androidColors[i], playerArrayList.get(i).player.getName(), false, 1));
        }
        infoip.setText(appContext.server.getIpAddress());
        return player_list;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        appContext.server.onDestroy();
    }

    @Override
    public void addPlayersInterfase(final ArrayList<Server.ConnectionToClient> clientList) {
        playerArrayList=clientList;
        for (int i =0;i<playerArrayList.size();i++)
            appContext.server.sendToAll("agregar_jugador,"+playerArrayList.get(i).player.getName()+","+playerArrayList.get(i).player.getColor());
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                itemArrayAdapter.setItemList(clientList);
            }
        });
    }

    @Override
    public void moveClient(String move) {

    }
}
