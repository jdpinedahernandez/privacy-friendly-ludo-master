package org.secuso.parques.clientserver;

import android.util.Log;

import org.secuso.parques.logic.Player;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.concurrent.LinkedBlockingQueue;

public class Server {
	private ArrayList<ConnectionToClient> clientList;
	private LinkedBlockingQueue<Object> messages;
	private ServerSocket serverSocket;
	static final int socketServerPORT = 8080;
	public Interfase myListener;
	ApplicationContext appContext;
	public interface Interfase {
		void addPlayersInterfase(ArrayList<ConnectionToClient> clientList);
		void moveClient(String move);
	}
    public void changeListener(Interfase listener){
        this.myListener = listener;
    }
	public Server(Interfase myListenerr, final ApplicationContext appContext) {
		this.myListener = myListenerr;
		this.appContext=appContext;
		try {
			clientList = new ArrayList<ConnectionToClient>();
			messages = new LinkedBlockingQueue<Object>();
			serverSocket = new ServerSocket(socketServerPORT);

			Thread accept = new Thread() {
				public void run(){
					while(appContext.creandoPartida){
						try{
							Socket s = serverSocket.accept();

							for (int i =0;i<clientList.size();i++){
								if (clientList.get(i).socket.getInetAddress().equals(s.getInetAddress())){
									clientList.set(i,new ConnectionToClient(s));
								}
								else{
									clientList.add(new ConnectionToClient(s));
									break;
								}
							}
							if (clientList.size()==0){
								clientList.add(new ConnectionToClient(s));
							}
						}
						catch(IOException e){ e.printStackTrace(); }
						myListener.addPlayersInterfase(clientList);

					}
				}
			};

			accept.setDaemon(true);
			accept.start();

			Thread messageHandling = new Thread() {
				public void run(){
					while(true){
						try{
							Object message = messages.take();
							// Do some handling here...
							System.out.println("Message Received: " + message);
						}
						catch(InterruptedException e){ }
					}
				}
			};

			messageHandling.setDaemon(true);
			messageHandling.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public class ConnectionToClient {
		ObjectInputStream in;
		ObjectOutputStream out;
		Socket socket;
		Player player;
		Boolean exist = false;
		ConnectionToClient(final Socket socket) throws IOException {

			this.socket = socket;
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			int[] color_array ={13840175,0x9C27B0,0x3F51B5,0x4CAF50};
			player = new Player(0, color_array[clientList.size()], "jugador"+socket.getInetAddress(), false, 1);

			Thread read = new Thread(){
				public void run(){
					while(true){
						try{
							Object obj = in.readObject();
							messages.put(obj);
//							sendToAll("server:"+socket.getInetAddress());
							sendToAll(obj.toString());
							myListener.moveClient(obj.toString());
						}
						catch(IOException e){ e.printStackTrace(); } catch (InterruptedException e) {
							e.printStackTrace();
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						}
					}
				}
			};

			read.setDaemon(true); // terminate when main ends
			read.start();
		}

		public void write(Object obj) {
			try{
				out.writeObject(obj);
			}
			catch(IOException e){ e.printStackTrace(); }
		}
	}

	public void sendToOne(int index, Object message)throws IndexOutOfBoundsException {
		clientList.get(index).write(message);
	}
	public ArrayList<ConnectionToClient> getClientList() {
		return  clientList;
	}
	public void sendToAll(Object message){
		for(ConnectionToClient client : clientList)
			client.write(message);
	}
	public String getIpAddress() {
		String ip = "";
		try {
			Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
					.getNetworkInterfaces();
			while (enumNetworkInterfaces.hasMoreElements()) {
				NetworkInterface networkInterface = enumNetworkInterfaces
						.nextElement();
				Enumeration<InetAddress> enumInetAddress = networkInterface
						.getInetAddresses();
				while (enumInetAddress.hasMoreElements()) {
					InetAddress inetAddress = enumInetAddress
							.nextElement();

					if (inetAddress.isSiteLocalAddress()) {
/*						ip += "Server running at : "
								+ inetAddress.getHostAddress();*/
						ip +=  inetAddress.getHostAddress();
						Log.i("server runninf at:",ip);
					}
				}
			}

		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ip += "Something Wrong! " + e.toString() + "\n";
		}
		return ip;
	}
	public void onDestroy() {
		if (serverSocket != null) {
			try {
				serverSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
