package org.secuso.parques.clientserver;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;

public class ClientThread extends Thread {

    private ConnectionToServer server;
    private LinkedBlockingQueue<Object> messages;
    public Socket socket;
    public String serverAddr;
    String addr;
    int port;
    public InterfaseClient listener;

    ClientThread(String addr, int port,InterfaseClient listener) {
        this.listener=listener;
        this.addr=addr;
        this.port=port;
    }
    public interface InterfaseClient {
        void InterfaseClientT(String mensaje);

    }
    public void changeListener(InterfaseClient listener){
        this.listener = listener;
    }
    @Override
    public void run() {
        for (int i = 0;i<256;i++){

            try {
                socket = new Socket();
                socket.connect(new InetSocketAddress("192.168.0."+i, port), 500);
                System.out.println("Connected: h" + addr);
                if (server==null){
                    serverAddr="192.168.0."+i;
                    server = new ConnectionToServer(socket);
                    messages = new LinkedBlockingQueue<Object>();

                    System.out.println("Connected: " + addr);

                    while (true) {
                        try {
                            Object message = messages.take();

                            System.out.println("Message Received: " + message);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }
                break;
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }

    }
    private class ConnectionToServer {
        ObjectInputStream in;
        ObjectOutputStream out;
        Socket socket;

        ConnectionToServer(Socket socket)  {
            try {
                this.socket = socket;
                in = new ObjectInputStream(socket.getInputStream());
                out = new ObjectOutputStream(socket.getOutputStream());

                Thread read = new Thread(){
                    public void run(){
                        while(true){
                            try{
                                final Object obj = in.readObject();
                                messages.put(obj);
                                listener.InterfaseClientT(obj.toString());
                            }
                            catch(IOException e)
                            { e.printStackTrace(); }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            } catch (ClassNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                };

                read.setDaemon(true);
                read.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void write(Object obj) {
            try{
                out.writeObject(obj);
            }
            catch(IOException e){ e.printStackTrace(); }
        }


    }

    public void send(final Object obj) {
        Thread read = new Thread(){
            public void run(){
                try {
                    server.write(obj);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        read.start();

    }
}

