package org.secuso.parques.clientserver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.secuso.parques.R;
import org.secuso.parques.activities.GameActivity;
import org.secuso.parques.logic.Player;

import java.util.ArrayList;

public class UnirPartida extends Activity implements ClientThread.InterfaseClient{

    RecyclerView recyclerView;
    Button unirme_e_iniciar;
    EditText txt_enviar;
    ApplicationContext appContext;

    ItemArrayAdapterClient itemArrayAdapter;
    ClientThread clientThread;
    private ArrayList<Player> player = new ArrayList<>();
    String myname="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unir_partida);
        ArrayList<Player> itemList = new ArrayList<>();
        appContext = (ApplicationContext) getApplicationContext();
        itemArrayAdapter = new ItemArrayAdapterClient(R.layout.list_item2, itemList);
        recyclerView = (RecyclerView) findViewById(R.id.item_list);
        unirme_e_iniciar = (Button) findViewById(R.id.enviar);
        txt_enviar = (EditText) findViewById(R.id.txt_enviar);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(itemArrayAdapter);

        Thread thread = null;
        try {
            appContext.clientThread = new ClientThread("192.168.0.", 8080,this);
            thread = new Thread(appContext.clientThread);
            thread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        unirme_e_iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UnirPartida.this, GameActivity.class);
                intent.putParcelableArrayListExtra("Players", player);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                appContext.clientThread.send(""+txt_enviar.getText());
            }
});
    }


    @Override
    public void InterfaseClientT(String mensaje) {
        appContext.myplayer_name="jugador"+appContext.clientThread.socket.getLocalAddress();
        String[]split=mensaje.split(",");
        int[] androidColors = getBaseContext().getResources().getIntArray(R.array.playerColors);
        boolean exist=false;
        if (split[0].equalsIgnoreCase("agregar_jugador")){
            if (player.size()==0){
                player.add(new Player(player.size(),R.color.black, "servidor_"+appContext.clientThread.serverAddr, false, 1));
            }
            for (int i =0;i<player.size();i++){
                if (player.get(i).getName().equalsIgnoreCase(split[1])){
                    exist=true;
                    break;
                }
            }
            if (!exist)
                player.add(new Player(player.size(),androidColors[player.size()-1], split[1], false, 1));

            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    itemArrayAdapter.setItemList(player);
                }
            });
        }
        else if (split[0].equalsIgnoreCase("iniciar")){
            Intent intent = new Intent(UnirPartida.this, GameActivity.class);
            intent.putParcelableArrayListExtra("Players", player);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }
}
