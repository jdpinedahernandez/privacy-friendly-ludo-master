package org.secuso.parques.clientserver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.secuso.parques.R;

public class ModoJuego extends Activity {

	Button btn_crear;
	Button btn_unirme;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main2);
		btn_crear = (Button) findViewById(R.id.btn_crear);
		btn_unirme = (Button) findViewById(R.id.btn_unirme);
		btn_crear.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(ModoJuego.this, CrearPartida.class);
				startActivity(intent);

			}
		});
		btn_unirme.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(ModoJuego.this, UnirPartida.class);
				startActivity(intent);

			}
		});
	}

	
}