package org.secuso.parques.clientserver;

import android.app.Application;

public class ApplicationContext extends Application {
    String myplayer_name=null;
    Server server=null;
    public ClientThread clientThread=null;
    public Boolean creandoPartida=true;

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }

    public ClientThread getClientThread() {
        return clientThread;
    }

    public void setClientThread(ClientThread clientThread) {
        this.clientThread = clientThread;
    }

    public String getMyplayer_name() {
        return myplayer_name;
    }

    public void setMyplayer_name(String myplayer_name) {
        this.myplayer_name = myplayer_name;
    }

    public Boolean getCreandoPartida() {
        return creandoPartida;
    }

    public void setCreandoPartida(Boolean creandoPartida) {
        this.creandoPartida = creandoPartida;
    }
}
